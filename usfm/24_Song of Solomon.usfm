\id SNG
\mt1 CANTIQUE DES CANTIQUE
\c 1
\p
\v 1 Cantique des cantiques, qui est de Salomon.
\v 2 Qu'il m'embrasse des baisers de sa bouche ; car tes mamelles sont meilleures que le vin.
\v 3 Et la senteur de tes parfums l'emporte sur tous les aromates ; ton nom est l'essence de parfum : voilà pourquoi les jeunes filles t'aiment.
\v 4 Elles t'attirent, et à ta suite nous courons aux senteurs de tes parfums. Le roi m'a introduite dans sa chambre secrète ; nous tressaillirons, nous nous réjouirons en toi ; nous aimerons tes mamelles plus que le vin ; la droiture te chérit.
\v 5 Filles de Jérusalem, je suis noire, et je suis belle, comme les tentes de Cédar, comme les tentures de Salomon.
\v 6 Ne me dédaignez pas, parce que je suis noire ; c'est que le soleil a altéré ma couleur. Les fils de ma mère se sont levés contre moi ; ils m'avaient fait la gardienne des vignes, et ma propre vigne, je ne l'aie point gardée.
\v 7 Ô toi que mon âme a aimé, indique-moi où tu pais ton troupeau, où tu reposes à midi, de peur que peut-être je ne m'égare à la suite des troupeaux de tes compagnons.
\v 8 Ô toi, la plus belle des femmes, si tu ne te connais toi-même, suis les traces de mes troupeaux, et pais tes chevreaux près des tentes de mes pasteurs.
\v 9 Ma bien-aimée, je t'ai comparée à mes cavales attelées aux chars de Pharaon.
\v 10 Tes joues, elles sont belles comme la colombe, et ton cou, comme un collier.
\v 11 Nous te ferons des ornements d'or émaillés d'argent.
\v 12 Aussi longtemps que le roi a été à table, mon nard a répandu son parfum.
\v 13 Mon frère bien-aimé est pour moi un bouquet de myrrhe ; il reposera entre mes mamelles.
\v 14 Mon frère bien-aimé est pour moi une grappe en fleur dans la vigne d'Engaddi.
\v 15 Que tu es belle, ô ma bien-aimée, que tu es belle ! Tes yeux sont ceux d'une colombe.
\v 16 Et toi, que tu es beau, mon frère bien-aimé ! Que tu es charmant à l'ombre de notre couche !
\v 17 Les solives de vos demeures sont de cèdre, et ses lambris de cyprès.
\c 2
\p
\v 1 Je suis la fleur des champs et le lis des vallées.
\v 2 Comme le lis des vallées au milieu des épines, ainsi est ma bien-aimée au milieu des jeunes filles.
\v 3 Comme le pommier parmi les arbres de la forêt, ainsi est mon frère bien- aimé parmi les jeunes hommes. J'ai désiré son ombre, et je m'y suis assise, et son fruit est doux à mon palais.
\v 4 Introduisez-moi dans le cellier au vin ; faites-y venir ma bien-aimée.
\v 5 Soutenez-moi avec des parfums ; entourez-moi de fruits, car je suis blessée d'amour.
\v 6 Sa main gauche sera sous ma tête, et de sa droite il m'embrassera.
\v 7 Filles de Jérusalem, je vous adjure, par les puissances et les vertus de la campagne, n'éveillez pas, ne réveillez pas ma bien-aimée, qu'elle-même ne le désire.
\v 8 C'est la voix de mon bien-aimé ; le voilà qui vient en bondissant sur les monts, en franchissant les collines.
\v 9 Mon frère bien-aimé ressemble au chevreuil ou au jeune faon sur les montagnes de Béthel. le voilà derrière notre mur ; il se penche par la fenêtre, il regarde à travers le treillis.
\v 10 Mon frère bien-aimé me parle, et me dit : Lève-toi, ma bien-aimée ; viens, ô ma belle, ô ma colombe.
\v 11 Car voilà que l'hiver est passé ; la pluie s'en est allée, elle est partie.
\v 12 Les fleurs se montrent sur la terre ; le temps de tailler est venu ; le roucoulement de la tourterelle s'entend sur notre terre.
\v 13 Les jeunes figues montrent leurs bourgeons ; les vignes sont en fleur et donnent leur parfum. Lève-toi, ma bien-aimée ; viens ô ma belle, ô ma colombe. Viens,
\v 14 toi ma colombe, à l'abri sous les roches, dans le creux des murs. Montre- moi ton visage, que j'entende ta voix ; car ta voix est douce, et ton visage est plein de grâces.
\v 15 Prenez les jeunes renards, qui ravagent les vignes, quand nos vignes sont en fleur.
\v 16 Mon frère bien-aimé est à moi, et moi à lui ; et il fait paître son troupeau parmi les lis,
\v 17 jusqu'à ce que se lèvent les premières brises du jour et que les ténèbres se dissipent. Reviens, mon frère bien-aimé ; accours comme le chevreuil ou le jeune faon sur les ravins des montagnes.
\c 3
\p
\v 1 Sur ma couche, la nuit, j'ai désiré celui qu'aime mon âme ; je l'ai cherché, et ne l'ai point trouvé ; je l'ai appelé, et il ne m'a pas écouté.
\v 2 Je me lèverai donc ; j'irai par la ville, les marchés, les places, et je chercherai celui qu'aime mon âme. Je l'ai cherché, et ne l'ai point trouvé.
\v 3 Les gardiens m'ont rencontrée, en faisant la ronde dans la ville : N'avez- vous pas vu celui qu'aime mon âme ?
\v 4 À peine éloignée d'eux, j'ai trouvé celui qu'aime mon âme ; je l'ai pris par la main, et ne l'ai point quitté que je ne l'eusse introduit dans la maison de ma mère, dans la chambre de celle qui m'a conçue.
\v 5 Filles de Jérusalem, je vous adjure par les puissances et les vertus des campagnes, n'éveillez pas, ne réveillez pas ma bien-aimée, qu'elle ne le désire.
\v 6 Qui est celle qui monte du désert, comme une colonne de fumée sortant de l'encens et de la myrrhe, et de toutes les poudres dont se composent les parfums ?
\v 7 Voilà la couche de Salomon ; soixante vaillants des forts d'Israël sont rangés en cercle autour d'elle.
\v 8 Tous exercés aux combats, ils ont l'épée au côté, pour en écarter les terreurs de la nuit.
\v 9 Le roi Salomon s'est fait un lit de cèdres du Liban.
\v 10 Les montants en sont d'argent, et le siège est d'or ; le marchepied est de porphyre, et l'intérieur est pavé de pierres précieuses, amour des filles de Jérusalem.
\v 11 Filles de Sion, sortez, contemplez le roi Salomon avec la couronne que lui a posée sa mère le jour de son mariage, le jour de la joie de son cœur.
\c 4
\p
\v 1 Que tu es belle, ô ma bien-aimée ! Que tu es belle ! Tes yeux sont ceux des colombes, sans parler de tes beautés cachées. Ta chevelure est comme la toison des troupeaux de chèvres qu'on voit en Galaad.
\v 2 Tes dents sont comme la laine des brebis sortant du lavoir après la tonte ; toutes ont deux petits, et nulle n'est stérile.
\v 3 Tes lèvres sont comme un ruban écarlate, et ton langage est plein de grâce ; tes joues sont comme la peau de la grenade, sans parler de tes beautés cachées.
\v 4 Ton cou est comme la tour de David, qu'il a bâtie pour être un arsenal ; mille boucliers y sont suspendus, et tous les dards des vaillants.
\v 5 Tes deux mamelles sont comme deux faons jumeaux du chevreuil, paissant parmi les lis,
\v 6 jusqu'à ce que se lèvent les premières brises du jour, et que les ténèbres se dissipent. J'irai à la montagne de myrrhe et à la colline d'encens.
\v 7 Tu es toute belle, ô ma bien-aimée ; et il n'est point de tache en toi.
\v 8 Viens du Liban, mon épouse, viens du Liban ; tu iras du puits du serment aux cimes de Sanir et d'Hermon, des antres des lions aux montagnes des panthères.
\v 9 Tu m'as ravi mon cœur, ma sœur, mon épouse ; tu m'as ravi mon cœur d'un seul de tes regards, d'un seul des cheveux de ton cou.
\v 10 Que tes mamelles sont belles, ma sœur, mon épouse ! Tes mamelles sont plus précieuses que le vin ; et les senteurs de tes vêtements plus douces que tous les aromates.
\v 11 De tes lèvres découlent des rayons de miel, ô mon épouse ; le miel et le lait sont sous ta langue, et l'odeur de tes vêtements est comme l'odeur de l'encens.
\v 12 Ma sœur, mon épouse, est un jardin enclos ; c'est un jardin enclos, une fontaine scellée.
\v 13 Tes rejetons forment un jardin de grenades avec les fruits du noyer, des grappes en fleur et du nard,
\v 14 du nard et du safran, de la canne et du cinnamome, de tous les arbres du Liban, de la myrrhe, de l'aloès, et des parfums les plus exquis.
\v 15 La fontaine de ce jardin est un puits d'eau vive, qui jaillit du Liban.
\v 16 Fuis d'ici, vent du nord ; viens, vent du midi, souffle sur mon jardin, et que mes parfums en découlent. Que mon frère bien-aimé descende en son jardin, qu'il mange de ses fruits.
\c 5
\p
\v 1 Je suis entré dans mon jardin, ma sœur, mon épouse ; j'ai récolté ma myrrhe et mes parfums ; j'ai mangé mon pain avec un rayon de miel ; j'ai bu mon vin et mon lait. mangez, ô mes amis ; buvez, mes frères, et enivrez-vous.
\v 2 Pour moi, je dors, et mon cœur veille. C'est la voix de mon frère bien-aimé ; il heurte à la porte : Ouvre-moi, dit-il, ma bien-aimée, ma sœur, ma colombe, ma parfaite ; ma tête est pleine de rosée, et mes cheveux humides des gouttes de la nuit.
\v 3 J'ai ôté ma tunique ; comment la revêtirai-je ? Je me suis lavé les pieds ; comment puis-je maintenant les salir ?
\v 4 Mon frère bien-aimé a passé la main dans l'ouverture de la porte, et si près de mon bien-aimé mes entrailles ont tressailli.
\v 5 Je me suis levée pour ouvrir à mon frère bien-aimé ; mes mains ont distillé la myrrhe ; mes doigts ont rempli de myrrhe la poignée du verrou.
\v 6 Et j'ai ouvert à mon frère bien-aimé, et mon frère bien-aimé n'était plus là ; et mon âme était défaillante pendant qu'il parlait. Je l'ai cherché, et ne l'ai point trouvé ; je l'ai appelé, et il ne m'a point entendue.
\v 7 Les gardes m'ont rencontrée, en faisant la ronde dans la ville ; ils m'ont battue et meurtrie ; les sentinelles des remparts m'ont dépouillée de mon manteau.
\v 8 Filles de Jérusalem, je vous adjure par les puissances et les vertus des campagnes, si vous avez trouvé mon frère bien-aimé, que lui direz-vous ? Dites- lui que je suis blessée d'amour.
\v 9 Qu'est donc ton frère bien-aimé auprès d'un autre frère, ô toi belle entre toutes les femmes ? Qu'est ton frère bien-aimé auprès d'un autre frère, pour que tu nous adjure ainsi ?
\v 10 Mon frère bien-aimé est blanc et rose, choisi entre dix mille.
\v 11 Sa tête est de l'or fin de Céphaz ; ses cheveux sont souples, et noirs comme le corbeau.
\v 12 Ses yeux sont comme ceux des colombes sur des étangs pleins d'eau, lavées dans le lait, se reposant sur les étangs.
\v 13 Ses joues sont comme des vases d'aromates exhalant des parfums ; ses lèvres sont des lis distillant la myrrhe la plus pure.
\v 14 Ses mains sont gracieuses et d'or, et pleines de béryl ; sa poitrine est une tablette d'ivoire sur une pierre de saphir.
\v 15 Ses jambes sont des colonnes de marbre posées sur des bases d'or ; sa beauté est celle du Liban : elle excelle comme le cèdre.
\v 16 Sa bouche est pleine de douceur, et fait naître les désirs ; tel est mon frère bien-aimé, tel est mon ami, filles de Jérusalem.
\c 6
\p
\v 1 Où est allé ton frère bien-aimé, ô toi belle entre toutes les femmes ? Où ton frère bien-aimé s'est-il retiré ? Et nous le chercherons avec toi.
\v 2 Mon frère bien-aimé est descendu dans son jardin parmi les roses d'aromates, pour paître son troupeau dans ses jardins, et y cueillir des lis.
\v 3 Je suis à mon frère bien-aimé, et mon frère bien-aimé est à moi ; il fait paître son troupeau parmi les lis.
\v 4 Tu es belle, ô ma bien-aimée, comme la tendresse, gracieuse comme Jérusalem, terrible comme une armée en bataille.
\v 5 Détourne de moi tes yeux, car ils m'ont transporté ; ta chevelure est comme les troupeaux de chèvres que l'on voit en Galaad.
\v 6 Tes dents sont comme la toison des brebis sortant du lavoir après la tonte, qui toutes ont deux petits, et dont nulle n'est stérile.
\v 7 Tes lèvres sont comme un ruban d'écarlate, et ton langage est plein de grâce. Tes joues sont comme la peau de la grenade, sans parler de tes beautés cachées.
\v 8 Il y a soixante reines et quatre-vingt concubines, et des jeunes filles sans nombre.
\v 9 Mais ma colombe, ma parfaite est unique ; elle est l'unique de sa mère ; elle est l'élue de celle qui l'a enfantée. Les jeunes filles l'ont vue, et la déclarent heureuse ; les reines et les concubines la loueront.
\v 10 Quelle est celle qui regarde dehors comme le matin ; belle comme la lune, élue comme le soleil, terrible comme une armée en bataille ?
\v 11 Je suis descendu au jardin des noyers, pour voir les fruits du vallon ; si la vigne est en fleur, et si les grenades sont fleuries.
\v 12 Là je te donnerai mes mamelles. Mon âme n'a rien su, et j'ai été comme emportée par les chers d'Aminadab.
\c 7
\p
\v 1 Reviens, reviens, ma Sulamite ; reviens, reviens, et nous ne verrons que toi. Que verrez-vous en la Sulamite, qui vient comme les rangs d'une armée ?
\v 2 Fille de Nadab, que les sandales donnent grâce à tes pas ! Les contours de tes jambes ressemblent à des colliers, chef-d'œuvre d'un artiste.
\v 3 Ton nombril est comme un cratère fait au tour, où le vin ne manque jamais. Ta poitrine est comme un monceau de froment enveloppé de lis.
\v 4 Tes deux mamelles sont comme les deux faons jumeaux d'un chevreuil.
\v 5 Ton cou est comme une tour d'ivoire, tes yeux comme des étangs d'Hésebon, près des portes de la fille issue de beaucoup de peuples. Ton nez est comme la tour du Liban qui regarde Damas.
\v 6 Ta tête est posée sur toi semblable au Mont Carmel, et ta chevelure est comme de la pourpre ; le roi est retenu dans tes galeries.
\v 7 Ô ma bien-aimée, que de grâce, que de suavité en tes délices !
\v 8 Ta stature est celle d'un palmier ; tes mamelles sont comme des grappes de raisins.
\v 9 J'ai dit : Je monterai au palmier ; j'atteindrai à son faîte, et tes mamelles seront comme les grappes de la vigne, l'odeur de tes narines comme celle des pommes,
\v 10 et ton gosier comme un bon vin. Qu'aime mon frère bien-aimé, et qui plaît à mes dents et à mes lèvres ;
\v 11 Je suis à mon frère bien-aimé, et son regard est tourné vers moi.
\v 12 Viens, mon frère bien-aimé, allons aux champs, dormons dans les bourgades.
\v 13 Levons-nous dès l'aurore pour aller aux vignes ; voyons si le raisin est en fleur, si les grains sont en fleur, si les grenades fleurissent : c'est là que je te donnerai mes mamelles.
\v 14 Les mandragores ont donné leur senteur, et, au seuil de nos portes, tous les fruits verts et mûrs, ô mon frère bien-aimé, je les ai gardés pour toi.
\c 8
\p
\v 1 Ô mon frère bien-aimé, que n'as-tu sucé les mamelles de ma mère ! Si je te trouve dehors, je te baiserai, et nul ne me méprisera.
\v 2 Je te prendrai par la main, je t'introduirai dans la maison de ma mère, dans la chambre de celle qui m'a conçu. Je te ferai boire du vin parfumé, et du jus de mes grenades.
\v 3 Il posera sa main gauche sur ma tête, et, de la droite, il m'embrassera.
\v 4 Je vous adjure, filles de Jérusalem, par les vertus de la campagne, n'éveillez pas, ne réveillez pas ma bien-aimée qu'elle ne le désire.
\v 5 Quelle est celle qui se lève blanche comme l'aubépine, appuyée sur son frère bien-aimé ? Je t'ai réveillée sous un pommier ; c'est là que ta mère t'a enfantée ; c'est là que t'a enfantée celle qui t'a conçu.
\v 6 Mets-moi comme un sceau sur ton cœur, et comme un sceau sur ton bras. Car l'amour est fort comme la mort, et la jalousie est cruelle comme l'enfer ; ses traits sont des traits de feu, ce sont des flammes.
\v 7 Des torrents d'eau ne pourront point éteindre l'amour, ni les fleuves le submerger. Si un homme donne toute sa vie par amour, les autres hommes auront pour lui le dernier mépris.
\v 8 Notre sœur est petite, et n'a point de mamelle ; que ferons-nous pour notre sœur, le jour où je viendrai lui parler ?
\v 9 Si elle est un mur, couronnons-la de créneaux d'argent ; si elle est une porte, incrustons-la de bois de cèdre.
\v 10 Moi, je suis un mur, et mes mamelles sont comme des tours ; et j'ai été à leurs yeux comme ayant trouvé la paix.
\v 11 Salomon avait une vigne en Béelamon ; il a donné sa vigne à ceux qui la gardent ; chacun rendra de ses fruits mille sicles d'argent.
\v 12 Ma vigne est à moi ; elle est devant mes yeux ; que Salomon en ait mille sicles ; que ceux qui gardent ses fruits en aient deux cents.
\v 13 Ô toi qui demeures dans les jardins, mes compagnes écoutent ta voix ; fais-la-moi entendre.
\v 14 Fuis, mon frère bien-aimé ; et, rapide comme le chevreuil ou comme le faon de la biche, fuis dans les montagnes des parfums.
